# PBcode - README #
---

### Overview ###

The **PBcode** tool is a code snippet manager where you can store, edit and search your source code with some descriptions.

### Screenshots ###

![PBcode - Start window](development/readme/pbcode1.png "PBcode - Start window")

![PBcode - Description window](development/readme/pbcode2.png "PBcode - Description window")

![PBcode - Code window](development/readme/pbcode3.png "PBcode - Code window")

![PBcode - Search window](development/readme/pbcode4.png "PBcode - Search window")

![PBcode - About window](development/readme/pbcode5.png "PBcode - About window")

### Setup ###

* Start the **PBcode.exe** executable with a doubleclick.
* Enter your code snippets and save the changes :-)
* Please also check the user manual!

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBcode** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
